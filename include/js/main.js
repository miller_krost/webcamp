(function($,undefined){
    $(document).ready(function(){
        //
        var nodeWrap = $(".bEventDetails .js_wrap"),
            nodeBg = $(".bEventDetailsBg"),
            nodeClose = $(".eEventDetails_closeWrap");
        //
        var closeFunc = function(){
            nodeWrap.parent().attr("hidden",true);
            nodeBg.attr("hidden",true)
        };
        nodeBg.on("click", closeFunc);
        nodeClose.on("click", closeFunc);
        //
        $(".bEventsWrap .bEvent").on("click", function(event){
            var node = $(event.delegateTarget);
            //
            nodeWrap.html( node.clone() );
            //
            nodeWrap.parent().removeAttr("hidden");
            nodeBg.removeAttr("hidden");
        });
    });
})(jQuery);
